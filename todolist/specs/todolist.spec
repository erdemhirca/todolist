ToDoList Dogrulama
====================================================

Listeye yapilacak is ekleme
----------------------------------------------------

*Listenin bos oldugunu kontrol et
*Item listesine "Proje yapilacak" itemini ekle
*Item listesinde "Proje yapilacak" iteminin eklendigini kontrol et

Listeye birden fazla yapilacak is ekle ve kontrol et
----------------------------------------------------

*Item listesinde "Proje yapilacak" iteminin oldugunu kontrol et
*Item listesine "Bilgisayari al" itemini ekle
*Item listesinde "Bilgisayari al" itemi "Proje yapilacak" iteminin altinda mi

Listeye radio butonu isaretleme
----------------------------------------------------

*Item listesinde "Proje yapilacak" ve "Bilgisayari al" iteminin oldugunu kontrol et
*Listedeki "Proje yapilacak" yanindaki radio butonuna tikla
*Listedeki "Proje yapilacak" itemi isaretlendi mi

Listeden radio butonu isaretini kaldirma
----------------------------------------------------

*Item listesinde "Proje yapilacak" iteminin isaretli oldugunu kontrol et
*Listedeki "Proje yapilacak" yanindaki radio butonuna tikla
*Listedeki "Proje yapilacak" itemi isaretsiz mi

Listeye item ekleme ve silme kontrolu
----------------------------------------------------

*Item listesinde "Proje yapilacak" ve "Bilgisayari al" iteminin oldugunu kontrol et
*Item listesine "Herseyi duzenli yap" itemini ekle
*Item listesinde "Herseyi duzenli yap" iteminin eklendigini kontrol et
*Listedeki "Proje yapilacak" iteminin silme butonuna tikla
*Item listesinde "Proje yapilacak" silindi mi